import 'package:flutter/material.dart';

void main () {
  runApp(new MaterialApp(home: new application()));
}

class application extends StatefulWidget {
  @override
  _applicationState createState() => _applicationState();
}

class _applicationState extends State<application> {

  void dialog() {
    showDialog(
        context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text('Title'),
          content: new Text('Alert Body'),
          actions: <Widget>[
            new IconButton(icon: new Icon(Icons.close), onPressed: () {Navigator.pop(context);})
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new RaisedButton(
            onPressed: () {dialog();},
          child: new Text('Activate'),
        ),
      )
    );
  }
}
